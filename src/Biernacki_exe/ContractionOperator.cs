﻿using proj_v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace proj_v1_exe
{
    public class ContractionOperator : IOperator
    {

        public void Perform(IGenome genome)
        {

            PipelineGenome pg = genome as PipelineGenome;

            if (pg.PipesCount <= 1)
                return;

            int index = RandomGenerator.Next(0, pg.PipesCount) * PipelineGenome.PipelinePrecision; //pierwszy indeks losowego rurociągu
            for (int i = index; i < (pg.PipesCount - 1) * PipelineGenome.PipelinePrecision; i++)
                pg.Genes[i] = pg.Genes[i + PipelineGenome.PipelinePrecision];

            pg.PipesCount--;
        }

        public double Rate { get; set; }

        public ContractionOperator(double rate)
        {
            Rate = rate;
        }
    }
}
