﻿using proj_v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace proj_v1_exe
{
    public class ExpansionOperator : IOperator
    {

        public void Perform(IGenome genome)
        {

            PipelineGenome pg = genome as PipelineGenome;
            
            if (pg.PipesCount == 10)
                return;

            for (int i = pg.PipesCount; i < pg.PipesCount + PipelineGenome.PipelinePrecision; i++)
                pg.Genes[i] = RandomGenerator.NextDouble() <= 0.5;

            pg.PipesCount++;

           // for (int i = 0; i < PipelineGenome.PipelinePrecision; ++i)
           //     genome.Genes.Add(RandomGenerator.NextDouble() <= 0.5);

            // copy one, random pipeline:
            // int index = RandomGenerator.Next(0, genome.Genes.Count / PipelineGenome.PipelinePrecision);
            // for (int i = index * PipelineGenome.PipelinePrecision; i < (index + 1) * PipelineGenome.PipelinePrecision; i++)
            //     genome.Genes.Add(genome.Genes[i]);
            //modified it a little
           //  for (int i = genome.Genes.Count - 8; i < genome.Genes.Count; i++)
             //    genome.Genes[i] = RandomGenerator.NextDouble() <= 0.5;
        }

        public double Rate { get; set; }

        public ExpansionOperator(double rate)
        {
            Rate = rate;
        }
    }
}
