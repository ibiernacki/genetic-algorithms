﻿using proj_v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace proj_v1_exe
{
    /// <summary>
    /// Crossover Method for "Pipelines" project.
    /// </summary>
    public class PipelineCrossover : ICrossover
    {
        /// <summary>
        /// Crossovers two genomes.
        /// </summary>
        /// <param name="genomeA">The parent a.</param>
        /// <param name="genomeB">The parent b.</param>
        public bool Crossover(IGenome genomeA, IGenome genomeB)
        {
            PipelineGenome childA = new PipelineGenome();
            PipelineGenome childB = new PipelineGenome();

            PipelineGenome parentA = genomeA as PipelineGenome;
            PipelineGenome parentB = genomeB as PipelineGenome;

            if(parentA.PipesCount == 1 || parentB.PipesCount == 1)
                return false;

            int crossoverPoint = RandomGenerator.Next(1, Math.Min(parentA.PipesCount, parentB.PipesCount)) * PipelineGenome.PipelinePrecision;

            for (int j = 0; j < crossoverPoint; ++j)
            {
                childA.Genes[j] = genomeA.Genes[j];
                childB.Genes[j] = genomeB.Genes[j];
            }

            for(int j = crossoverPoint; j < 10*PipelineGenome.PipelinePrecision; j++)
            {
                childA.Genes[j] = genomeB.Genes[j];
                childB.Genes[j] = genomeA.Genes[j];
            }

            genomeA = childA;
            genomeB = childB;

            return true;

        }
    }
}
