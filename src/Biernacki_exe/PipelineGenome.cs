﻿using proj_v1;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace proj_v1_exe
{
    public class Pipeline
    {
        public double Start { get; private set; }
        public double End { get; private set; }

        public double X1 { get; private set; }
        public double X2 { get; private set; }
        public double Y1 { get; private set; }
        public double Y2 { get; private set; }


        public double PipelineLength
        {
            get
            {
                return Math.Sqrt(Math.Pow(X2 - X1, 2) + Math.Pow(Y2 - Y1, 2));
            }
        }

        private enum Order
        {
            Left,
            Top,
            Right,
            Bottom
        }

        private void ParseOrder(Order[] order)
        {
            Order border = order[0];
            double end = End;

            if (End < 1.0 / 3.0)
                border = order[0];
            else if (End < 2.0 / 3.0)
            {
                border = order[1];
                end -= 1.0 / 3.0;
            }
            else
            {
                border = order[2];
                end -= 2.0 / 3.0;
            }

            if (border == Order.Left)
            {
                X2 = 0.0;
                Y2 = end;
            }
            else if (border == Order.Top)
            {
                X2 = end;
                Y2 = 0.0;
            }
            else if (border == Order.Right)
            {
                X2 = 1.0;
                Y2 = end;
            }
            else if (border == Order.Bottom)
            {
                X2 = end;
                Y2 = 1.0;
            }
        }

        public Pipeline(double start, double end)
        {
            Start = start;
            End = end;

            Order[] order = new Order[3];

            if (Start < 0.25) //Lewa krawędź
            {
                X1 = 0.0;
                Y1 = Start / 0.25;
                ParseOrder(new Order[] { Order.Top, Order.Right, Order.Bottom });
            }
            else if (Start < 0.5) //Górna
            {
                X1 = (Start - 0.25) / 0.25;
                Y1 = 0.0;
                ParseOrder(new Order[] { Order.Right, Order.Bottom, Order.Left });
            }
            else if (Start < 0.75) //Prawa
            {
                X1 = 1.0;
                Y1 = (Start - 0.5) / 0.25;
                ParseOrder(new Order[] { Order.Bottom, Order.Left, Order.Top });
            }
            else //Dolna
            {
                X1 = (Start - 0.75) / 0.25;
                Y1 = 1.0;
                ParseOrder(new Order[] { Order.Left, Order.Top, Order.Right });
            }
        }
    }

    /// <summary>
    /// Fenotype contains from 1 to 10 pipes.
    /// </summary>
    public class PipelineGenome : GenomeBase<Pipeline[]>
    {
        public static readonly int PipelinePrecision = 32;
        public static readonly int PointPrecision = 16;

        /// <summary>
        /// Gets or sets the pipes count.
        /// </summary>
        /// <value>
        /// The pipes count.
        /// </value>
        public int PipesCount { get; set; }

        public override Pipeline[] Fenotype
        {
            get
            {
                Pipeline[] pipelines = new Pipeline[PipesCount];
                int index = 0;
                for (int i = 0; i < PipesCount*PipelineGenome.PipelinePrecision; i += PipelineGenome.PipelinePrecision)
                {
                    Int64 start = 1;
                    for (int j = i; j < i + PipelineGenome.PointPrecision; j++)
                        if (Genes[j])
                            start += 2 << j;

                    Int64 end = 1;
                    for (int j = i + PipelineGenome.PointPrecision; j < i + PipelineGenome.PipelinePrecision; j++)
                        if (Genes[j])
                            end += 2 << (j - (i + PipelineGenome.PointPrecision));

                    double sStart = (double)start /  (double)(2 << PointPrecision);
                    double sEnd = (double)end /  (double)(2 << PointPrecision);

                    pipelines[index++] = new Pipeline(sStart, sEnd);
                }
                return pipelines;
            }
        }

        public override void InitiateRandom()
        {
            PipesCount = RandomGenerator.Next(1, 11);
            Genes.SetAll(false);

            for (int i = 0; i < PipesCount * PipelineGenome.PipelinePrecision; i++)
                Genes[i] = RandomGenerator.NextDouble() <= 0.5;
        }

        public override IGenome Clone()
        {
            PipelineGenome genome = new PipelineGenome();
            genome.Genes = new BitArray(this.Genes);
            genome.PipesCount = this.PipesCount;
            return genome;
        }

        /// <summary>
        /// New PipelineGenome instance.
        /// </summary>
        public PipelineGenome()
        {
            Genes = new BitArray(PipelineGenome.PipelinePrecision*10);
        }

    }
}
