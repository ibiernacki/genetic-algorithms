﻿using proj_v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace proj_v1_exe
{
    public class PipelineMutation : IMutation
    {

        public void Mutate(IGenome genome)
        {
            PipelineGenome pg = genome as PipelineGenome;
            int n = RandomGenerator.Next(1, 8); //how many mutations: range 1-7
            for (int i = 0; i < n; i++)
            {
              
                int m = RandomGenerator.Next(0, (pg.PipesCount*PipelineGenome.PipelinePrecision) / PipelineGenome.PointPrecision) * PipelineGenome.PointPrecision; // starting point
                for (int j = m; j < m + PipelineGenome.PointPrecision; j++)
                    genome.Genes[j] = RandomGenerator.NextDouble() <= 0.5;
            }
        }
    }

    public class PipelineMutation2 : IMutation
    {
        public void Mutate(IGenome genome)
        {
            int n = RandomGenerator.Next(1, 9);
            for (int i = 0; i < n; i++)
            {
                int m = RandomGenerator.Next(0, genome.Genes.Count / PipelineGenome.PointPrecision) * PipelineGenome.PointPrecision; //mutated point
                int mm = RandomGenerator.Next(1, 11); //amount of mutated bits
                for (int j = m + PipelineGenome.PointPrecision - mm; j < m + PipelineGenome.PointPrecision; j++)
                    genome.Genes[j] = RandomGenerator.NextDouble() <= 0.5;
            }
        }
    }

    public class PipelineMutation3 : IMutation
    {
        public void Mutate(IGenome genome)
        {
            int n = RandomGenerator.Next(5, Math.Min(genome.Genes.Count, 160));
            for(int i = 0; i < n; i++)
                genome.Genes[RandomGenerator.Next(0, genome.Genes.Count)] = RandomGenerator.NextDouble() <= 0.5;
        }
    }
}
