﻿using proj_v1;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;

namespace proj_v1_exe
{
    public class City
    {
        public double X;
        public double Y;
        public double Radius;
        public City(double x, double y, double radius)
        {
            X = x;
            Y = y;
            Radius = radius;
        }
    }

    public class PipelineObjective : IObjective
    {
        public ReadOnlyCollection<City> Cities { get; private set; }
        private static double MaxPipelineLength = 0.0;

        public double GetDistance(Pipeline p, City c)
        {
            double d = c.X * (p.Y2 - p.Y1) + c.Y * (p.X1 - p.X2) + p.X2 * p.Y1 - p.X1 * p.Y2;
            d = Math.Abs(d);
            d /= Math.Sqrt(Math.Pow(p.Y1 - p.Y2, 2) + Math.Pow(p.X1 - p.X2, 2));
            return d;
        }

        public double GetDistance(City c, PipelineGenome g)
        {
            double min_distance = double.PositiveInfinity;
            foreach (var p in g.Fenotype)
            {
                double dist = GetDistance(p, c);
                if (dist < min_distance)
                {
                    min_distance = dist;
                    if (dist <= c.Radius)
                        return 0;
                }
            }
            return min_distance;
        }


        public bool IsConnected(Pipeline p, City c)
        {
            return GetDistance(p, c) <= c.Radius;
        }

        public bool IsConnected(City c, Pipeline[] pipelines)
        {
            foreach (var p in pipelines)
                if (IsConnected(p, c))
                    return true;
            return false;
        }

        public int ConnectedCitiesCount(Pipeline[] pipelines)
        {
            int connected = 0;
            foreach (City c in Cities)
                connected += IsConnected(c, pipelines) ? 1 : 0;
            return connected;
        }


        public double GetFitness(IGenome genome)
        {
            if (!(genome is PipelineGenome))
                throw new ArgumentException("Genome is not PipelineGenome");

            PipelineGenome pipelineGenome = genome as PipelineGenome;

            Pipeline[] pipelines = pipelineGenome.Fenotype;
            double connected = ConnectedCitiesCount(pipelines);
            double pipelineLength = 0.0;

            foreach (Pipeline p in pipelines)
                pipelineLength += p.PipelineLength;


            if (pipelineLength > MaxPipelineLength)
                MaxPipelineLength = pipelineLength;

            return 1.1 * connected / Cities.Count + (MaxPipelineLength - pipelineLength) / MaxPipelineLength;
        }


        public PipelineObjective()
        {
            List<City> cities = new List<City>();

            var lines = File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "\\" + "param.ini");
            for (int i = 1; i < lines.Length; i++)
            {
                var values = lines[i].Split(' ');
                cities.Add(new City(double.Parse(values[0], CultureInfo.InvariantCulture), double.Parse(values[1], CultureInfo.InvariantCulture), double.Parse(values[2], CultureInfo.InvariantCulture)));
            }
            Cities = new ReadOnlyCollection<City>(cities);
        }
    }
}
