﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using proj_v1;
using System.IO;
using System.Globalization;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace proj_v1_exe
{

    class Program
    {
        static void PrintStatistics(Stopwatch sw, int generation)
        {
            Console.WriteLine("{0:00}:{1:00}\t{2}\tGenerations", sw.Elapsed.Minutes, sw.Elapsed.Seconds, generation);
        }

        static void Main(string[] args)
        {
            CultureInfo culture = CultureInfo.InvariantCulture;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            PipelineObjective objective = new PipelineObjective();
            PipelineGenome Best = new PipelineGenome();

            Best.InitiateRandom();
            int populations = 0;

         //   while (sw.ElapsedMilliseconds < 15 * 1000)
         //   {
            Population<PipelineGenome> p = new Population<PipelineGenome>(objective, 1000, new RankSelection(), new PipelineMutation(), new PipelineCrossover());
            p.pMutation = 0.5;
            p.pCrossover = 0.2;


            p.CustomOperators.Add(new ExpansionOperator(0.6));
            p.CustomOperators.Add(new ContractionOperator(0.6));

            for (int i = 0; i < 100; i++ )
            {
                p.Step();
                populations++;
                p.pMutation -= 0.15 / 100.0;
                p.pCrossover += 0.35 / 100.0;
                
                if (objective.GetFitness(Best) < objective.GetFitness(p.BestEver))
                    Best = p.BestEver;
            }
            PrintStatistics(sw, populations);

            for (int i = 0; i < 200; i++ )
            {
                p.Step();
                populations++;
                p.pMutation -= 0.05 / 200.0;
                p.pCrossover += 0.35 / 200.0;

                if (objective.GetFitness(Best) < objective.GetFitness(p.BestEver))
                    Best = p.BestEver;
                if (i == 99)
                    PrintStatistics(sw, populations);
            }


            PrintStatistics(sw, populations);

            while(sw.ElapsedMilliseconds < 1000*180)
            {
                p.Step();
                if (objective.GetFitness(Best) < objective.GetFitness(p.BestEver))
                    Best = p.BestEver;
                populations++;
                
                if (populations % 100 == 0)
                    PrintStatistics(sw, populations);

            }

            sw.Stop();
            
            string output = String.Format(culture, "{0}\n", Best.Fenotype.Length);
            foreach (var pipe in Best.Fenotype)
                output += String.Format(culture, "{0} {1} {2} {3}\n", pipe.X1, pipe.Y1, pipe.X2, pipe.Y2);
            
            double length = 0.0;
            foreach (var pipe in Best.Fenotype)
                length += pipe.PipelineLength;

            output += String.Format(culture, "{0} {1}", objective.ConnectedCitiesCount(Best.Fenotype), length);
           
            Console.WriteLine(output);
            File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "\\" + "Biernacki.txt", output);

            PrintStatistics(sw, populations);
        }
    }
}
