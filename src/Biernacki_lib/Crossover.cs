﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace proj_v1
{
    /// <summary>
    /// Defines Crossover method.
    /// </summary>
    public interface ICrossover 
    {
        /// <summary>
        /// Crossovers two genomes.
        /// </summary>
        /// <param name="genomeA">The parent a.</param>
        /// <param name="genomeB">The parent b.</param>
        /// <returns>Returns true if crossover succeeded</returns>
        bool Crossover(IGenome genomeA, IGenome genomeB);
    }

    
}
