﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace proj_v1
{


    /// <summary>
    /// Genome definition. Contains zero-one based genotype and Fenotype.
    /// </summary>
    public interface IGenome : IComparable
    {
        /// <summary>
        /// Gets list of genes.
        /// </summary>
        /// <value>
        /// The genes.
        /// </value>
        BitArray Genes { get; }
        /// <summary>
        /// Gets or sets fitness value.
        /// </summary>
        /// <value>
        /// The fitness.
        /// </value>
        double Fitness { get; set; }

        /// <summary>
        /// Returns clone of this IGenome instance.
        /// </summary>
        /// <returns></returns>
        IGenome Clone();

        /// <summary>
        /// Initializes Genome with random genes.
        /// </summary>
        void InitiateRandom();
    }

    /// <summary>
    /// Base class for all Genomes.
    /// </summary>
    /// <typeparam name="T">Fenotype's type</typeparam>
    public abstract class GenomeBase<T> : IGenome
    {
        private BitArray _Genes { get; set; }
        /// <summary>
        /// Gets list of genes.
        /// </summary>
        /// <value>
        /// The genes.
        /// </value>
        public BitArray Genes
        {
            get
            {
                if (_Genes == null)
                    _Genes = new BitArray(32);
                return _Genes;
            }
            protected set
            {
                _Genes = value;
            }
        }
        /// <summary>
        /// Gets the fenotype.
        /// </summary>
        /// <value>
        /// The fenotype.
        /// </value>
        public abstract T Fenotype { get; }
        /// <summary>
        /// Gets or sets fitness value.
        /// </summary>
        /// <value>
        /// The fitness.
        /// </value>
        public double Fitness { get; set; }

        /// <summary>
        /// Initializes Genome with random genes.
        /// </summary>
        public virtual void InitiateRandom() { }
        /// <summary>
        /// Returns clone of this IGenome instance.
        /// </summary>
        /// <returns></returns>
        public abstract IGenome Clone();

        /// <summary>
        /// Compares the current instance with another object of the same type and returns an integer that indicates whether the current instance precedes, follows, or occurs in the same position in the sort order as the other object.
        /// </summary>
        /// <param name="obj">An object to compare with this instance.</param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has these meanings: Value Meaning Less than zero This instance precedes <paramref name="obj" /> in the sort order. Zero This instance occurs in the same position in the sort order as <paramref name="obj" />. Greater than zero This instance follows <paramref name="obj" /> in the sort order.
        /// </returns>
        /// <exception cref="System.ArgumentException">Object is not an IGenome implementation.</exception>
        public virtual int CompareTo(object obj)
        {
            if (obj == null) return 1;

            IGenome genome = obj as IGenome;
            if (genome != null)
                return this.Fitness.CompareTo(genome.Fitness);
            else
                throw new ArgumentException("Object is not an IGenome implementation.");
        }
    }
    
}
