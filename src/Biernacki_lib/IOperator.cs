﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace proj_v1
{
    /// <summary>
    /// Genetic operator
    /// </summary>
    public interface IOperator
    {
        /// <summary>
        /// Performs the operation on specified genome.
        /// </summary>
        /// <param name="genome">The genome which will be modified by this operator</param>
        void Perform(IGenome genome);

        /// <summary>
        /// Gets the operator's perform rate.
        /// </summary>
        /// <value>
        /// The rate that operator will be performed.
        /// </value>
        double Rate { get; }
    }
}
