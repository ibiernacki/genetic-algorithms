﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace proj_v1
{
    /// <summary>
    /// Defines Mutation method.
    /// </summary>
    public interface IMutation
    {
        /// <summary>
        /// Mutates the specified genome.
        /// </summary>
        /// <param name="genome">The genome to mutate</param>
        void Mutate(IGenome genome);
    }

   
}
