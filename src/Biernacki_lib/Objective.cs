﻿
namespace proj_v1
{
    /// <summary>
    /// Problem's objective. Contains fitness function.
    /// </summary>
    public interface IObjective
    {
        /// <summary>
        /// Returns genome's fitness (value must be positive)
        /// </summary>
        /// <returns>Fitness value (must be positive)</returns>
        double GetFitness(IGenome genome);
    }
}
