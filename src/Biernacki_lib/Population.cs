﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace proj_v1
{

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IPopulation<T> where T : class, IGenome, new()
    {
        /// <summary>
        /// Gets the size of the population.
        /// </summary>
        /// <value>
        /// The size of the population.
        /// </value>
        int PopulationSize { get; }

        /// <summary>
        /// Gets or sets the rate of mutation.
        /// </summary>
        /// <value>
        /// The rate of mutation in range (0.0; 1.0)
        /// </value>
        double pMutation { get; set; }
       
        /// <summary>
        /// Gets or sets the rate of crossover.
        /// </summary>
        /// <value>
        /// The rate of crossover in range (0.0; 1.0)
        /// </value>
        double pCrossover { get; set; }

        /// <summary>
        /// Gets the crossover method.
        /// </summary>
        /// <value>
        /// The crossover method.
        /// </value>
        ICrossover Crossover { get; }

        /// <summary>
        /// Gets the mutation method.
        /// </summary>
        /// <value>
        /// The mutation method.
        /// </value>
        IMutation Mutation { get; }

        /// <summary>
        /// Gets the selection method.
        /// </summary>
        /// <value>
        /// The selection method.
        /// </value>
        ISelection Selection { get; }

        /// <summary>
        /// Gets or sets amount of best individuals which will always pass to the next generation without any changes 
        /// </summary>
        /// <value>
        /// The elitism value in range (0.0; 1.0)
        /// </value>
        double Elitism { get; set; }

        /// <summary>z
        /// Gets the Collection of genomes  in current state of population.
        /// </summary>
        /// <value>
        /// The Collection of genomes in current state of population.
        /// </value>
        List<T> Genomes { get; }

        /// <summary>
        /// Gets the best genome ever found in all generations.
        /// </summary>
        /// <value>
        /// The best genome.
        /// </value>
        T BestEver { get; }

        /// <summary>
        /// Gets the objective.
        /// </summary>
        /// <value>
        /// The objective.
        /// </value>
        IObjective Objective { get; }

        /// <summary>
        /// Gets the custom operators.
        /// </summary>
        /// <value>
        /// Custom operators.
        /// </value>
        IList<IOperator> CustomOperators { get; }

        /// <summary>
        /// Evolves population to next generation. Performs all genetic operators.
        /// </summary>
        void Step();

    }

    /// <summary>
    /// Base class for Genetic Algorithm's library. Keeps all genomes, performs genetic operators.
    /// </summary>
    /// <typeparam name="T">Genome type</typeparam>
    public class Population<T> : IPopulation<T> where T : class, IGenome, new()
    {
        /// <summary>
        /// Gets the Collection of genomes in current state of population.
        /// </summary>
        /// <value>
        /// The Collection of genomes in current state of population.
        /// </value>
        public List<T> Genomes { get; private set; }

        /// <summary>
        /// Gets the objective.
        /// </summary>
        /// <value>
        /// The objective.
        /// </value>
        public IObjective Objective { get; private set; }


        /// <summary>
        /// Gets the size of the population.
        /// </summary>
        /// <value>
        /// The size of the population.
        /// </value>
        public int PopulationSize { get; private set; }

        private double _pMutation = 0.05;
        /// <summary>
        /// Gets or sets the rate of mutation.
        /// </summary>
        /// <value>
        /// The rate of mutation in range (0.0; 1.0)
        /// </value>
        public double pMutation
        {
            get { return _pMutation; }
            set { _pMutation = Math.Min(value, 1.0); }
        }

        private double _pCrossover = 0.8;
        /// <summary>
        /// Gets or sets the rate of crossover.
        /// </summary>
        /// <value>
        /// The rate of crossover in range (0.0; 1.0)
        /// </value>
        public double pCrossover
        {
            get { return _pCrossover; }
            set { _pCrossover = Math.Min(value, 1.0); }
        }

        private double _Elitism = 0.05;
        /// <summary>
        /// Gets or sets percantage of best individuals which will always pass to the next generation: range [0; 0.5], default: 0.1
        /// </summary>
        /// <value>
        /// The elitism value in range (0.0; 1.0)
        /// </value>
        public double Elitism
        {
            get { return _Elitism; }
            set { _Elitism = Math.Min(value, 0.5); }
        }


        /// <summary>
        /// Gets the best genome ever found in all generations.
        /// </summary>
        /// <value>
        /// The best genome.
        /// </value>
        public T BestEver { get; protected set; }


        /// <summary>
        /// Gets the crossover method.
        /// </summary>
        /// <value>
        /// The crossover method.
        /// </value>
        public ICrossover Crossover { get; protected set; }
        /// <summary>
        /// Gets the mutation method.
        /// </summary>
        /// <value>
        /// The mutation method.
        /// </value>
        public IMutation Mutation { get; protected set; }
        /// <summary>
        /// Gets the selection method.
        /// </summary>
        /// <value>
        /// The selection method.
        /// </value>
        public ISelection Selection { get; protected set; }

        /// <summary>
        /// Gets the custom operators.
        /// </summary>
        /// <value>
        /// The custom operator.
        /// </value>
        public IList<IOperator> CustomOperators { get; private set; }

        /// <summary>
        /// Evolves population to next generation. Performs all genetic operators.
        /// </summary>
        public void Step()
        {
            //ELITE
            List<T> elite = new List<T>((int)Elitism * Genomes.Count);
            List<T> elite2 = new List<T>((int)Elitism * Genomes.Count);
            elite.Add(Genomes[0].Clone() as T);
            for (int i = 0; i < Elitism * Genomes.Count - 1; ++i)
            {
                elite.Add(Genomes[i].Clone() as T);
                elite2.Add(Genomes[i].Clone() as T);
            }

            //SELECTION
            List<T> newGeneration = Selection.Select<T>(Genomes, (int)(Genomes.Count - 2* Elitism * Genomes.Count));
            newGeneration.InsertRange(0, elite2);


            //MUTATION
            for (int i = 0; i < newGeneration.Count; ++i)
            {
                if (RandomGenerator.NextDouble() >= _pMutation)
                    continue;
                Mutation.Mutate(newGeneration[i]);
            }

            //CROSSOVER
            for (int i = 0; i < newGeneration.Count - newGeneration.Count % 2; i += 2)
            {
                T parentA = newGeneration[i];
                T parentB = newGeneration[i + 1];

                if (RandomGenerator.NextDouble() > pCrossover)// || parentA.Genes.Count <= 32 || parentB.Genes.Count <= 32)
                    continue;

                T childA = parentA.Clone() as T;
                T childB = parentB.Clone() as T;

                Crossover.Crossover(childA, childB);

                newGeneration[i] = childA;
                newGeneration[i + 1] = childB;
            }

            foreach(IOperator customOperator in CustomOperators)
            {
                double r = 0.0;
                foreach(T genome in Genomes)
                {
                    r = RandomGenerator.NextDouble();
                    if (customOperator.Rate <= r)
                        customOperator.Perform(genome);
                }
            }

            Parallel.ForEach<T>(newGeneration, new Action<T>(g => g.Fitness = Objective.GetFitness(g)));
            newGeneration.InsertRange(0, elite);
            Genomes = newGeneration;

            Genomes.Sort((a, b) => b.CompareTo(a));
            BestEver.Fitness = Objective.GetFitness(BestEver);

            if (BestEver.Fitness < Genomes[0].Fitness)
                BestEver = Genomes[0].Clone() as T;

        }

        /// <summary>
        /// Initializes the empty population.
        /// </summary>
        private void InitializeEmptyPopulation()
        {
            Genomes = new List<T>();

            for (int i = 0; i < PopulationSize; ++i)
            {
                T genome = new T();
                genome.InitiateRandom();
                Genomes.Add(genome);
            }

            Genomes.ForEach(g => g.Fitness = Objective.GetFitness(g));
            Genomes.Sort((a, b) => b.CompareTo(a));
            BestEver = Genomes[0];
            BestEver.Fitness = Objective.GetFitness(BestEver);
        }

        

        /// <summary>
        /// Creates new instance of a Population. Genome type is <typeparamref name="T" />
        /// </summary>
        /// <param name="objective">Objective - used to calculate fitness function value.</param>
        /// <param name="populationSize">Size of a population</param>
        /// <param name="selection">Genetic operator for Selection method</param>
        /// <param name="mutation">Genetic operator for Mutation method</param>
        /// <param name="crossover">Genetic operator for Crossover method</param>
        public Population(IObjective objective, int populationSize, ISelection selection, IMutation mutation, ICrossover crossover)
        {
            Objective = objective;
            PopulationSize = populationSize;
            Selection = selection;
            Crossover = crossover;
            CustomOperators = new List<IOperator>();
            Mutation = mutation;
            
            InitializeEmptyPopulation();
            
        }
    }
}
