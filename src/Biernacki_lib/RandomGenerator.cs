﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace proj_v1
{
    /// <summary>
    /// Genetic Library's random numbers generator
    /// </summary>
    public interface IRandomGenerator
    {
        /// <summary>
        /// Returns random integer in range [min, max)
        /// </summary>
        /// <param name="min">inclusive minimum range value</param>
        /// <param name="max">exclusive maximum range value</param>
        /// <returns>Random int in range [min, max)</returns>
        int Next(int min, int max);

        /// <summary>
        /// Returns random double in range [0.0, 1.0]
        /// </summary>
        /// <returns>Random double in range (0.0, 1.0)</returns>
        double NextDouble();

    }

    /// <summary>
    /// Random Generator used in Genetic Algorithms' library.
    /// </summary>
    public static class RandomGenerator
    {
        /// <summary>
        /// Random Generator. By default it's StandardRandomGenerator.
        /// </summary>
        public static IRandomGenerator Generator = new StandardRandomGenerator();

        /// <summary>
        /// Returns random integer in range [min, max)
        /// </summary>
        /// <param name="min">inclusive minimum range value</param>
        /// <param name="max">exclusive maximum range value</param>
        /// <returns>Random int in range [min, max)</returns>
        public static int Next(int min, int max)
        {
            return Generator.Next(min, max);
        }

        /// <summary>
        /// Returns random double in range (0.0, 1.0)
        /// </summary>
        /// <returns>Random double in range (0.0, 1.0)</returns>
        public static double NextDouble()
        {
            return Generator.NextDouble();
        }
    }

    /// <summary>
    /// Implementation of IRandomGenerator which uses System.Random generator.
    /// </summary>
    public class StandardRandomGenerator : IRandomGenerator
    {
        private Random RandomNumbersGenerator = new Random();

        /// <summary>
        /// Returns random integer in range [min, max)
        /// </summary>
        /// <param name="min">inclusive minimum range value</param>
        /// <param name="max">exclusive maximum range value</param>
        /// <returns>
        /// Random int in range [min, max)
        /// </returns>
        public int Next(int min, int max)
        {
            return RandomNumbersGenerator.Next(min, max);
        }

        /// <summary>
        /// Returns random double in range [0.0, 1.0]
        /// </summary>
        /// <returns>
        /// Random double in range (0.0, 1.0)
        /// </returns>
        public double NextDouble()
        {
            return RandomNumbersGenerator.NextDouble();
        }
    }

    
}
