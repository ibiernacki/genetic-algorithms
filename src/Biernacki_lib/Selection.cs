﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace proj_v1
{
    /// <summary>
    /// Defines Selection method
    /// </summary>
    public interface ISelection
    {
        /// <summary>
        /// Applies selection and returns clones of specified amount of selected genomes.
        /// </summary>
        /// <typeparam name="T">Genome type.</typeparam>
        /// <param name="genomes">List of genomes sorted descending by Fitness value</param>
        /// <param name="size">Amound of genomes to select</param>
        /// <returns>Collection of selected genomes</returns>
        List<T> Select<T>(List<T> genomes, int size) where T : class, IGenome;
    }

    /// <summary>
    /// Rank Selection
    /// </summary>
    public class RankSelection : ISelection
    {

        /// <summary>
        /// Applies selection and returns clones specified amount of selected genomes.
        /// </summary>
        /// <typeparam name="T">Genome type.</typeparam>
        /// <param name="genomes">List of genomes sorted descending by Fitness value</param>
        /// <param name="size">Amound of genomes to select</param>
        /// <returns>
        /// Collection of selected genomes
        /// </returns>
        public List<T> Select<T>(List<T> genomes, int size) where T : class, IGenome
        {
            List<T> selected = new List<T>(size);

            int popSize = genomes.Count;
            double a = 0.1 / popSize;
            double k = 2 * (0.9) / popSize;

            double[] p_arr = new double[popSize];
            p_arr[0] = a + k;

            for (int i = 1; i < popSize; i++)
                p_arr[i] = p_arr[i - 1] + a + k * (1.0 - (double)(i) / (double)(popSize - 1));

            for (int i = 0; i < size; i++)
            {
                double r = RandomGenerator.NextDouble();
                selected.Add(genomes[BinarySearch(p_arr, 1, popSize - 1, r)].Clone() as T);
            }

            return selected;
        }

        private int BinarySearch(double[] arr, int lowBound, int highBound, double value)
        {
            if (value <= arr[0])
                return 0;

            int mid;
            while (lowBound <= highBound)
            {
                mid = (lowBound + highBound) / 2;
                if (arr[mid] < value)   //value: on the right
                {
                    lowBound = mid + 1;
                    continue;
                }
                else if (arr[mid - 1] > value)   //value: on the left
                {
                    highBound = mid - 1;
                    continue;
                }
                else  //value found
                    return mid;
            }

            return arr.Length - 1;  //if not found: return last index
        }
    }

    /// <summary>
    /// Defines Tournament Selection.
    /// </summary>
    public class TournamentSelection : ISelection
    {
        /// <summary>
        /// Applies selection and returns clones of specified amount of selected genomes.
        /// </summary>
        /// <typeparam name="T">Genome type.</typeparam>
        /// <param name="genomes">List of genomes sorted descending by Fitness value</param>
        /// <param name="size">Amound of genomes to select</param>
        /// <returns>
        /// Collection of selected genomes
        /// </returns>
        public List<T> Select<T>(List<T> genomes, int size) where T : class, IGenome
        {
            int popSize = genomes.Count;
            int tournamentSize = 10;
            int selectedCount = 0;
            double fitnessTotal = 0.0;

            foreach (var g in genomes)
                fitnessTotal += g.Fitness;

            List<T> selected = new List<T>(size);
            double[] p_arr = new double[popSize];

            p_arr[0] = genomes[0].Fitness / fitnessTotal;
            for (int i = 1; i < popSize; i++)
                p_arr[i] = p_arr[i - 1] + genomes[i].Fitness / fitnessTotal;


            List<T> tournament = new List<T>(tournamentSize);

            //just fill list
            for (int i = 0; i < tournamentSize; i++)
                tournament.Add(genomes[0]);

            while (selectedCount < size)
            {
                int bestIndex = 0;
                for (int i = 0; i < tournamentSize; i++)
                {
                    double r = RandomGenerator.NextDouble();
                    tournament[i] = genomes[BinarySearch(p_arr, 1, popSize - 1, r)];
                    if (tournament[i].Fitness > tournament[bestIndex].Fitness)
                        bestIndex = i;
                }

                selected.Add(tournament[bestIndex].Clone() as T);
                selectedCount++;
            }

            return selected;
        }

        private int BinarySearch(double[] arr, int lowBound, int highBound, double value)
        {
            if (value <= arr[0])
                return 0;

            int mid;
            while (lowBound <= highBound)
            {
                mid = (lowBound + highBound) / 2;
                if (arr[mid] < value)   //value: on the right
                {
                    lowBound = mid + 1;
                    continue;
                }
                else if (arr[mid - 1] > value)   //value: on the left
                {
                    highBound = mid - 1;
                    continue;
                }
                else  //value found
                    return mid;
            }

            return arr.Length - 1;  //if not found: return last index
        }
    }
}
