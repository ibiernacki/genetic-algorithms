﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using OxyPlot.Axes;
using proj_v1;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using OxyPlot.Series;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using System.Windows;
using System.Threading;

namespace proj_v1_data
{

    [Serializable]
    public class DataInfo : IDataPoint
    {
        public double X { get; set; }
        public double Y { get; set; }

        public string ProgramName { get; set; }
        public double pMutation { get; set; }
        public double pCrossover { get; set; }
        public double pResize { get; set; }
        public int CitiesCount { get; set; }
        public string Solution { get; set; }
        public string ParamIni { get; set; }
        public double ConnectedCities { get; set; }
        public double PipelineLength { get; set; }
        public int Index { get; set; }
        public double PipesCount { get; set; }
        public String ParamIniName { get; set; }
        public String DataFileName { get; set; }
        public String AdditionalInfo { get; set; }

        public double ConnectedPercent { get { return (double)ConnectedCities / (double)CitiesCount * 100.0; } }

        public static string TrackerFormatString
        {
            get
            {
                string trackerString = "{DataFileName}" + Environment.NewLine;
                trackerString += "Exe Name: {ProgramName} | param: {ParamIniName}" + Environment.NewLine;
                trackerString += "Index: {Index}" + Environment.NewLine;
              //  trackerString += "Parameters: " + Environment.NewLine;
              //  trackerString += "pMutation: {pMutation:0.##}" + Environment.NewLine;
              //  trackerString += "pCrossover: {pCrossover:0.##}" + Environment.NewLine;
              //  trackerString += "Expansion/Contraction: {pResize:0.##}" + Environment.NewLine;
                trackerString += "Connected Cities: {ConnectedCities}/{CitiesCount} ({ConnectedPercent:0.#}%)" + Environment.NewLine;
                trackerString += "Pipeline Length: {PipelineLength:0.##} | Pipes Count: {PipesCount:0.##}" + Environment.NewLine;
                trackerString += "X: {X:0.#####} | Y: {Y:0.#####}" + Environment.NewLine;
                trackerString += "{AdditionalInfo}";
                return trackerString;
            }
        }

        public DataInfo(int index, string programName, double pMutation, double pCrossover, double pResize, string solution, string paramIniName)
        {
            this.Index = index;
            this.ProgramName = programName;
            this.pMutation = pMutation;
            this.pCrossover = pCrossover;
            this.pResize = pResize;
            this.Solution = solution;
            this.ParamIniName = paramIniName;
            this.DataFileName = String.Empty;

            String[] param_ini = File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "\\exe\\param.ini");
            CitiesCount = int.Parse(param_ini[0], CultureInfo.InvariantCulture);
            ParamIni = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "\\exe\\param.ini");

            String[] s = solution.Split('\n');//.Split(' ');
            int last = 1;
            if (String.IsNullOrWhiteSpace(s[s.Length - 1]))
                last++;
            PipesCount = int.Parse(s[0], CultureInfo.InvariantCulture);
            s = s[s.Length - last].Split(' ');
            X = double.Parse(s[1], CultureInfo.InvariantCulture);
            PipelineLength = X;

            ConnectedCities = int.Parse(s[0], CultureInfo.InvariantCulture);
            Y = 1 - (double)ConnectedCities / (double)CitiesCount;
        }

        public DataInfo(DataInfo source)
        {
            Index = source.Index;
            ProgramName = source.ProgramName;
            pMutation = source.pMutation;
            pCrossover = source.pCrossover;
            pResize = source.pResize;
            Solution = source.Solution;
            ConnectedCities = source.ConnectedCities;
            CitiesCount = source.CitiesCount;
            PipelineLength = source.PipelineLength;
            PipesCount = source.PipesCount;
            ParamIni = source.ParamIni;
            ParamIniName = source.ParamIniName;
            DataFileName = source.DataFileName;
            X = source.X;
            Y = source.Y;
        }

    }

    public class DataViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private BackgroundWorker DataWorker { get; set; }

        private Boolean _IsLegendVisible = false;
        public Boolean IsLegendVisible
        {
            get { return _IsLegendVisible; }
            set
            {
                _IsLegendVisible = value;
                OnPropertyChanged("IsLegendVisible");
                PlotModel.IsLegendVisible = value;
            }
        }

        private Boolean _ArgsEnabled = false;
        public Boolean ArgsEnabled
        {
            get { return _ArgsEnabled; }
            set
            {
                _ArgsEnabled = value;
                OnPropertyChanged("ArgsEnabled");
            }
        }

        private Boolean _Parameterless = true;
        public Boolean Parameterless
        {
            get { return _Parameterless; }
            set
            {
                _Parameterless = value;
                ArgsEnabled = !value;
                OnPropertyChanged("Parameterless");
            }
        }

        private Boolean _ParameterlessEnabled = true;
        public Boolean ParameterlessEnabled
        {
            get { return _ParameterlessEnabled; }
            set
            {
                _ParameterlessEnabled = value;
                OnPropertyChanged("ParameterlessEnabled");
            }
        }

        private Boolean _GenerateEnabled = true;
        public Boolean GenerateEnabled
        {
            get { return _GenerateEnabled; }
            set
            {
                _GenerateEnabled = value;
                OnPropertyChanged("GenerateEnabled");
            }
        }

        private Boolean _AllPoints = true;
        public Boolean AllPoints
        {
            get { return _AllPoints; }
            set
            {
                if (_AllPoints != value)
                {
                    _AllPoints = value;
                    if (value)
                    {
                        foreach (var s in DataSeries)
                            PlotModel.Series.Add(s);
                    }

                    if (!value)
                    {
                        foreach (var s in DataSeries)
                            PlotModel.Series.Remove(s);
                    }
                    if (PlotModel.PlotControl != null)
                        PlotModel.PlotControl.InvalidatePlot(true);
                    OnPropertyChanged("AllPoints");
                }
            }
        }

        private Boolean _MassCenters = true;
        public Boolean MassCenters
        {
            get { return _MassCenters; }
            set
            {
                if (_MassCenters != value)
                {
                    _MassCenters = value;

                    if (value)
                    {
                        foreach (var s in MassCentersSeries)
                            PlotModel.Series.Add(s);
                    }

                    if (!value)
                    {
                        foreach (var s in MassCentersSeries)
                            PlotModel.Series.Remove(s);
                    }

                    if (PlotModel.PlotControl != null)
                        PlotModel.PlotControl.InvalidatePlot(true);

                    OnPropertyChanged("MassCenters");
                }
            }
        }

        private String _Arg1 = "0.04 0.06";
        public String Arg1
        {
            get { return _Arg1; }
            set
            {
                _Arg1 = value;
                OnPropertyChanged("Arg1");
            }
        }

        private String _Arg2 = "0.33 0.6";
        public String Arg2
        {
            get { return _Arg2; }
            set
            {
                _Arg2 = value;
                OnPropertyChanged("Arg2");
            }
        }

        private String _Arg3 = "0.4";
        public String Arg3
        {
            get { return _Arg3; }
            set
            {
                _Arg3 = value;
                OnPropertyChanged("Arg3");
            }
        }

        private String _Title = "[AG] Project - comparison tool.";
        public String Title
        {
            get { return _Title; }
            set
            {
                _Title = value;
                OnPropertyChanged("Title");
            }
        }

        private double _ProgressValue = 0.0;
        public double ProgressValue
        {
            get { return _ProgressValue; }
            set
            {
                _ProgressValue = value;
                OnPropertyChanged("ProgressValue");
                OnPropertyChanged("ProgressString");
            }
        }

        private double _ProgressMax = 100.0;
        public double ProgressMax
        {
            get { return _ProgressMax; }
            set
            {
                _ProgressMax = value;
                OnPropertyChanged("ProgressMax");
            }
        }

        private int _SelectedIndex;
        public int SelectedIndex
        {
            get { return _SelectedIndex; }
            set
            {
                _SelectedIndex = value;
                OnPropertyChanged("SelectedIndex");
            }
        }

        public String ProgressString
        {
            get
            {
                return String.Format("{0}/{1} ({2:0.##}%)", ProgressValue, ProgressMax, ProgressValue / ProgressMax * 100);
            }
        }

        private List<DataInfo> _Data = new List<DataInfo>();
        public List<DataInfo> Data
        {
            get { return _Data; }
        }
        private List<DataInfo> _MassCentersData = new List<DataInfo>();
        public List<DataInfo> MassCentersData
        {
            get
            {
                return _MassCentersData;
            }
            set
            {
                _MassCentersData = value;
                OnPropertyChanged("MassCentersData");
            }
        }
        public List<ScatterSeries> MassCentersSeries { get; set; }
        public List<ScatterSeries> DataSeries { get; set; }

        private ObservableCollection<String> _ParamIniFilters = new ObservableCollection<string>(new string[] { "-" });
        public ObservableCollection<String> ParamIniFilters
        {
            get
            {
                return _ParamIniFilters;
            }
            private set
            {
                _ParamIniFilters = value;
                OnPropertyChanged("ParamIniFilters");
            }
        }

        private String _SelectedParamIniFilter = "-";
        public String SelectedParamIniFilter
        {
            get { return _SelectedParamIniFilter; }
            set
            {
                _SelectedParamIniFilter = value;

                if (String.IsNullOrEmpty(value) || value == "-")
                    FilterData(null);
                else
                    FilterData(di => di.ParamIniName == value);

                OnPropertyChanged("SelectedParamIniFilter");
            }
        }

        private void FilterData(Func<DataInfo, bool> filter)
        {
            PlotModel.Series.Clear();
            DataSeries.Clear();
            MassCentersSeries.Clear();
            MassCentersData.Clear();
            Data.Clear();

            PlotData(filter);

            if (!AllPoints)
            {
                _AllPoints = true;
                AllPoints = false;
            }

            if (!MassCenters)
            {
                _MassCenters = true;
                MassCenters = false;
            }

          //  Pareto();
            PlotModel.RefreshPlot(true);
        }


        private PlotModel _PlotModel = new PlotModel();
        public PlotModel PlotModel
        {
            get { return _PlotModel; }
            set
            {
                _PlotModel = value;
                OnPropertyChanged("PlotModel");
            }
        }

        public void GenerateData()
        {
            ProgressValue = 0.0;
            ArgsEnabled = false;
            ParameterlessEnabled = false;
            GenerateEnabled = false;

            DataWorker.RunWorkerAsync();

        }
        public void PlotData(Func<DataInfo, bool> filter = null)
        {

            IFormatter formatter = new BinaryFormatter();

            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\data"))
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\\data");

            var data_files = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory + "\\data", "*.bin", SearchOption.TopDirectoryOnly);

            foreach (var data_file in data_files)
            {

                Stream stream = new FileStream(data_file, FileMode.Open, FileAccess.Read, FileShare.Read);
                List<DataInfo> data = (List<DataInfo>)formatter.Deserialize(stream);
                if (filter != null)
                    data = new List<DataInfo>(data.Where(filter));
                stream.Close();

                if (data == null || data.Count == 0)
                    continue;

                int count = Data.Count;
                Data.AddRange(data);
                for (int i = count; i < Data.Count; i++)
                {
                    Data[i].Index = i;
                    Data[i].DataFileName = Path.GetFileName(data_file);
                }

                ScatterSeries massCenters = new ScatterSeries();
                massCenters.MarkerType = MarkerType.Circle;
                massCenters.MarkerSize = 5;

                massCenters.Title = ""; // String.Format("Mass Center ({0})", data[0].ProgramName);
                massCenters.TrackerFormatString = DataInfo.TrackerFormatString;

                DataInfo massCenter = null;


                ScatterSeries dataSerie = new ScatterSeries();
                dataSerie.MarkerType = MarkerType.Circle;
                dataSerie.MarkerSize = 3;

                dataSerie.MouseDown += (s, e) =>
                {
                    var index = (int)e.HitTestResult.Index;
                    SelectedIndex = (dataSerie.Points[index] as DataInfo).Index;
                    e.Handled = false;
                };

                dataSerie.Title = String.Format("{0}", data[0].ProgramName);
                dataSerie.TrackerFormatString = DataInfo.TrackerFormatString;


                for (int i = 0; i < data.Count; i++)
                {
                    DataInfo d = data[i];

                    if (!ParamIniFilters.Contains(d.ParamIniName))
                        ParamIniFilters.Add(d.ParamIniName);

                    dataSerie.Points.Add(d);
                    if (massCenter == null)
                        massCenter = new DataInfo(d) { Solution = String.Empty, Index = -1, AdditionalInfo = String.Format("Indexes: {0}", d.Index) };
                    else
                    {
                        massCenter.ConnectedCities += d.ConnectedCities;
                        massCenter.PipelineLength += d.PipelineLength;
                        massCenter.PipesCount += d.PipesCount;
                        massCenter.X += d.X;
                        massCenter.Y += d.Y;
                        massCenter.AdditionalInfo += String.Format(", {0}", d.Index);
                    }
                }

                massCenter.ConnectedCities /= (double)data.Count;
                massCenter.PipesCount /= (double)data.Count;
                massCenter.X /= (double)data.Count;
                massCenter.Y /= (double)data.Count;
                massCenter.PipelineLength /= (double)data.Count;
                massCenters.Points.Add(massCenter);

                MassCentersData.Add(massCenter);
                PlotModel.Series.Add(dataSerie);
                DataSeries.Add(dataSerie);

                PlotModel.Series.Add(massCenters);
                MassCentersSeries.Add(massCenters);
            }
        }

        public void DrawSolution()
        {
            if (SelectedIndex < 0 || Data.Count <= SelectedIndex)
                return;
            SolutionWindow sw = new SolutionWindow(Data[SelectedIndex]);
            sw.Owner = App.Current.MainWindow;
            sw.ShowDialog();
        }

        public void Pareto()
        {
            List<DataInfo> pareto = new List<DataInfo>();

            DataInfo lowestX = MassCentersData[0];
            DataInfo lowestY = MassCentersData[0];

            foreach (DataInfo massCenter in MassCentersData)
            {
                if (massCenter.AdditionalInfo.Contains("First pareto front!"))
                    massCenter.AdditionalInfo = massCenter.AdditionalInfo.Substring(0, massCenter.AdditionalInfo.LastIndexOf('\n'));

                if (pareto.FirstOrDefault(di => di.X < massCenter.X && di.Y < massCenter.Y) == null)
                {
                    for (int i = 0; i < pareto.Count; i++)
                    {
                        if (pareto[i].X > massCenter.X && pareto[i].Y > massCenter.Y)
                        {
                            pareto.RemoveAt(i);
                            i--;
                        }
                    }
                    pareto.Add(massCenter);
                }

                if (lowestX.X > massCenter.X)
                    lowestX = massCenter;
                if (lowestY.Y > massCenter.Y)
                    lowestY = massCenter;
            }

            if (!pareto.Contains(lowestX))
                pareto.Add(lowestX);
            if (!pareto.Contains(lowestY))
                pareto.Add(lowestY);

            foreach (var p in pareto)
                p.AdditionalInfo += "\nFirst pareto front!";
        }

        public DataViewModel()
        {
            PlotModel.Axes.Add(new LinearAxis(AxisPosition.Bottom, 0.0, 10.0, "Długość wszystkich kanałów"));
            PlotModel.Axes.Add(new LinearAxis(AxisPosition.Left, -0.1, 1.1, "1 - (Liczba zasilonych / Liczba miast)"));

            PlotModel.IsLegendVisible = IsLegendVisible;
            PlotModel.SelectionColor = OxyColors.Red;

            DataWorker = new BackgroundWorker();
            DataWorker.DoWork += DataWorker_DoWork;
            DataWorker.RunWorkerCompleted += DataWorker_RunWorkerCompleted;

            DataSeries = new List<ScatterSeries>();
            MassCentersSeries = new List<ScatterSeries>();
            PlotData();
        }

        void DataWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ArgsEnabled = true;
            GenerateEnabled = true;
            FilterData(di => true);
        }
        void DataWorker_DoWork(object sender, DoWorkEventArgs e)
        {

            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\exe"))
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\\exe");
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\param"))
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\\param");

            var exes = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory + "\\exe", "*.exe", SearchOption.TopDirectoryOnly);
            {
                var txt_files = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory + "\\exe", "*.txt", SearchOption.TopDirectoryOnly);
                foreach (var fName in txt_files)
                    File.Delete(fName);
            }
            var params_ini = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory + "\\param", "*.ini", SearchOption.TopDirectoryOnly);
            CultureInfo culture = CultureInfo.InvariantCulture;



            IFormatter formatter = new BinaryFormatter();
            Stream stream = null;

            double[] mutations = new double[] { -1.0 };
            double[] crossovers = new double[] { -1.0 };
            double[] resizes = new double[] { -1.0 };

            if (!Parameterless)
            {
                try
                {
                    string[] mutations_str = Arg1.Split(' ');
                    string[] crossovers_str = Arg2.Split(' ');
                    string[] resizes_str = Arg3.Split(' ');

                    mutations = new double[mutations_str.Length];
                    crossovers = new double[crossovers_str.Length];
                    resizes = new double[resizes_str.Length];

                    for (int i = 0; i < mutations_str.Length; i++)
                        mutations[i] = double.Parse(mutations_str[i], culture);

                    for (int i = 0; i < crossovers_str.Length; i++)
                        crossovers[i] = double.Parse(crossovers_str[i], culture);

                    for (int i = 0; i < resizes_str.Length; i++)
                        resizes[i] = double.Parse(resizes_str[i], culture);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("InvalidArgs\n" + ex.Message, "Invalid Args", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            Dispatcher.CurrentDispatcher.Invoke(new Action(() => ProgressValue = 0));
            Dispatcher.CurrentDispatcher.Invoke(new Action(() => ProgressMax = exes.Length * params_ini.Length * mutations.Length * crossovers.Length * resizes.Length));

           
            int index = 0;
            foreach (var exe in exes)
            {
                foreach (var pMutation in mutations)
                {
                    foreach (var pCrossover in crossovers)
                    {
                        foreach (var pResize in resizes)
                        {
                            List<DataInfo> StoredData = new List<DataInfo>();

                            foreach (var param_ini in params_ini)
                            {
                                File.Copy(param_ini, AppDomain.CurrentDomain.BaseDirectory + "\\exe\\param.ini", true);


                                // String output = String.Empty;

                                ProcessStartInfo startInfo = new ProcessStartInfo();
                                //  startInfo.CreateNoWindow = true;
                                startInfo.UseShellExecute = false;
                                startInfo.FileName = exe;
                                startInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory + "\\exe";
                                startInfo.WindowStyle = ProcessWindowStyle.Minimized;

                                // if (!Parameterless)
                                //     startInfo.Arguments = String.Format(culture, "500 500 {0} {1}", pMutation, pCrossover);
                                // Console.WriteLine("{0} -> {1}", i, startInfo.Arguments);

                                Process process = new Process();
                                process.StartInfo = startInfo;
                                process.Start();
                                process.StartInfo.RedirectStandardOutput = true;

                                process.OutputDataReceived += (s, eArgs) =>
                                    {
                                        string receiveddata = eArgs.Data;
                                    };


                                int seconds = 0;
                                while (seconds < 180)
                                {
                                    if (process.HasExited)
                                        break;

                                    int remaining_minuts = (180 - seconds) / 60;
                                    int remaining_seconds = (180 - seconds) % 60;
                                    Title = String.Format("[AG] Project - comparison tool. [{0:00}:{1:00}]", remaining_minuts, remaining_seconds);
                                    seconds++;
                                    Thread.Sleep(1000);
                                }
                                if (seconds == 180)
                                {
                                    process.Kill();
                                    Title = "[AG] Project - comparison tool.";
                                    continue;
                                }
                                Title = "[AG] Project - comparison tool.";

                                String output = String.Empty;
                                String txt_path = Path.GetDirectoryName(exe) + "\\" + Path.GetFileNameWithoutExtension(exe) + ".txt";
                                String[] txt_files = Directory.GetFiles(Path.GetDirectoryName(exe), "*.txt");
                                if (File.Exists(txt_path))
                                {
                                    output = File.ReadAllText(txt_path);
                                    foreach (var fName in txt_files)
                                        File.Delete(fName);
                                }
                                else
                                {

                                    if (txt_files.Length == 0)
                                    {
                                        MessageBox.Show(String.Format("Failed to read output\nExe:{0}\nEstimated output file:{1}", exe, txt_path), "Failed to read output", MessageBoxButton.OK, MessageBoxImage.Error);
                                        return;
                                    }
                                    output = File.ReadAllText(txt_files[0]);
                                    foreach (var fName in txt_files)
                                        File.Delete(fName);
                                }


                                DataInfo data = new DataInfo(index++, Path.GetFileNameWithoutExtension(exe), pMutation, pCrossover, pResize, output, Path.GetFileNameWithoutExtension(param_ini));
                                StoredData.Add(data);
                                Dispatcher.CurrentDispatcher.Invoke(new Action(() => ProgressValue++));

                            }

                            //Save it here
                            stream = new FileStream(String.Format(AppDomain.CurrentDomain.BaseDirectory + "\\data\\{0:dd-MM HH-mm-ss} {1}.bin", DateTime.Now, Path.GetFileNameWithoutExtension(exe)), FileMode.Create, FileAccess.Write, FileShare.None);
                            formatter.Serialize(stream, StoredData);
                            stream.Close();
                        }
                    }

                }
            }

            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\data\\backup.bin"))
                File.Delete(AppDomain.CurrentDomain.BaseDirectory + "\\data\\backup.bin");
        }
    }
}
