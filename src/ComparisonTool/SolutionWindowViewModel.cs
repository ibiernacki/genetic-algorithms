﻿using OxyPlot;
using OxyPlot.Series;
using proj_v1;
using proj_v1_exe;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace proj_v1_data
{
    public class SolutionWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private PlotModel _PlotModel;
        public PlotModel PlotModel
        {
            get { return _PlotModel; }
            set
            {
                _PlotModel = value;
                OnPropertyChanged("PlotModel");
            }
        }

        private String _ParamIni;
        public String ParamIni
        {
            get { return _ParamIni; }
            set
            {
                _ParamIni = value;
                OnPropertyChanged("ParamIni");
            }
        }

        private String _Solution;
        public String Solution
        {
            get { return _Solution; }
            set
            {
                _Solution = value;
                OnPropertyChanged("Solution");
            }
        }

        private String _Info;
        public String Info
        {
            get { return _Info; }
            set
            {
                _Info = value;
                OnPropertyChanged("Info");
            }
        }
        

        public SolutionWindowViewModel(DataInfo info)
        {
            PlotModel = new OxyPlot.PlotModel();
            CultureInfo culture = CultureInfo.InvariantCulture;

            Solution = info.Solution;
            ParamIni = info.ParamIni;
            Info = info.ProgramName + Environment.NewLine;
            Info += info.ParamIniName + Environment.NewLine;
            Info += "Cities connected: " + info.ConnectedCities + Environment.NewLine;
            Info += "Cities total: " + info.CitiesCount + Environment.NewLine;
            Info += "Pipelines: " + info.PipesCount + Environment.NewLine;
            Info += "Pipeline total length: " + String.Format("{0:0.####}", info.PipelineLength);



            FunctionSeries s = new FunctionSeries();
            s.Color = OxyColor.Parse("200,160,180,123");
            s.StrokeThickness = 4;
            s.Points.Add(new DataPoint(0, 0));
            s.Points.Add(new DataPoint(1, 0));
            s.Points.Add(new DataPoint(1, 1));
            s.Points.Add(new DataPoint(0, 1));
            s.Points.Add(new DataPoint(0, 0));

            PlotModel.Series.Add(s);

            File.WriteAllText("param.ini", info.ParamIni);
            PipelineObjective objective = new PipelineObjective();

            foreach (var c in objective.Cities)
            {
                FunctionSeries serie = new FunctionSeries();
                serie.StrokeThickness = 4;
                serie.Color = OxyColor.Parse("255,232,44,12");
                serie.Smooth = true;

                double angle = 0.0;
                while (angle <= Math.PI * 2.0)
                {
                    double x = c.X + c.Radius * Math.Cos(angle);
                    double y = c.Y + c.Radius * Math.Sin(angle);
                    serie.Points.Add(new DataPoint(x, y));
                    angle += 0.05;
                }
                serie.Points.Add(new DataPoint(c.X + c.Radius, c.Y));
                if (c.Radius == 0)
                    serie.Points.Add(new DataPoint(c.X + 0.01, c.Y));

                PlotModel.Series.Add(serie);
            }

            Solution = Solution.TrimEnd('\n');

            String[] pipelines = Solution.Split('\n');

            for (int i = 1; i < pipelines.Length - 1; i++)
            {
                String pipeline = pipelines[i];
                String[] p = pipeline.Split(' ');
                FunctionSeries serie = new FunctionSeries();
                serie.Color = OxyColor.Parse("255,0,0,0");
                serie.Points.Add(new DataPoint(double.Parse(p[0], culture), double.Parse(p[1], culture)));
                serie.Points.Add(new DataPoint(double.Parse(p[2], culture), double.Parse(p[3], culture)));
                PlotModel.Series.Add(serie);
            }
        }
    }
}
